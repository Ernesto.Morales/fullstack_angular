import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map, catchError} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Persona } from '../Modelo/Persona';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  
  constructor(private httpClient:HttpClient) { }

  Url='http://localhost:8071/readAllData';
  Url2='http://localhost:8071/createData';
  Url3='http://localhost:8071/deleteData?id=';
  Url4='http://localhost:8071/deleteAllData?';
  Url5='http://localhost:8071/readData?id=';
  Url6='http://localhost:8071/updateData?id=';




  getPersonas(){

    return this.httpClient.get<Persona[]>(this.Url);
  }


  agregarEmpleado(empleado: any){
    let json = JSON.stringify(empleado);

    let headers = new HttpHeaders().set('Content-type', 'application/json');

    return this.httpClient.post(this.Url2, json, {headers:headers});
  }

  deletePersona(persona:Persona){
    return this.httpClient.delete<Persona>(this.Url3+persona.id);
  }

  DeletePersonas():Observable<any>{
    return this.httpClient.delete(this.Url4);
  }

  getPersonaId(id:number){
   

    return this.httpClient.get<Persona>(this.Url5+id);
  }

  updatePersona(persona:Persona){

    return this.httpClient.put<Persona>(this.Url6+persona.id,persona);
  }
}
