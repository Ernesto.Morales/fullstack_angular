import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from 'src/app/Service/service.service';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  agregarEmpleadoRegistro: any = 
  {id:'',
   data:{
    nombre:'',
    apellidoPat:'', 
    apellidoMat:'', 
    email:'',
    compania:'',
    telefono:'',
    genero:''}}
 
  
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit() {
  }


  Guardar(){
    console.log("evento agregar")

    this.service.agregarEmpleado(this.agregarEmpleadoRegistro).subscribe(resultado =>{
     
    }, 
    error=>{
      console.log(JSON.stringify(error));
    });

  }


  
}